<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
	<meta name="generator" content="LibreOffice 4.2.8.2 (Linux)">
	<meta name="created" content="0;0">
	<meta name="changedby" content="Roberto Scotti">
	<meta name="changed" content="20160502;94501703721474">
	<style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; line-height: 120% }
		pre.cjk { font-family: "Droid Sans Fallback", monospace }
		td p { margin-bottom: 0cm }
		th p { margin-bottom: 0cm }
	-->
	</style>
</head>
<body lang="it-IT" dir="ltr">
<pre class="western">Estimation of tree plantations growth and yield:</pre>
<pre class="western">A) douglas fir in Italy, with specific reference
to Tuscany</pre>
<pre class="western"><br>
</pre>
<table width="100%" cellpadding="3" cellspacing="0">
	<col width="19*">
	<col width="80*">
	<col width="54*">
	<col width="51*">
	<col width="51*">
	<thead>
		<tr valign="top">
			<th width="8%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">order</font></font></p>
			</th>
			<th width="31%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">module</font></font></p>
			</th>
			<td width="21%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">Input</font></font></p>
			</td>
			<td width="20%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">Output</font></font></p>
			</td>
			<th width="20%" style="border: 1px solid #000000; padding: 0.1cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">comment</font></font></p>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr valign="top">
			<td width="8%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">1</font></font></p>
			</td>
			<td width="31%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<pre class="western"><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">GrowthModel.R</font></font></pre>
			</td>
			<td width="21%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">DistDiam
				ed Anagrafica (Età, Fertilità) di diverse UC,</font></font></p>
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">sl
				= simulation length</font></font></p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">DistDiam
				e GrowthSynthesis0</font></font></p>
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">da
				Età iniziale e per sy anni dopo</font></font></p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">rapido</font></font></p>
			</td>
		</tr>
		<tr valign="top">
			<td width="8%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">2</font></font></p>
			</td>
			<td width="31%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">TimberAssormentsVolume.R</font></font></p>
			</td>
			<td width="21%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">DistDiam
				iniziale e finale</font></font></p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">GrowthSynthesis</font></font></p>
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">inizio
				e fine, con volumi e lunghezza per assortimento</font></font></p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">lento,
				cubatura con TapeR ==&gt; produzione tavola cub a doppia entrata
				locale</font></font></p>
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">25
				min</font></font></p>
			</td>
		</tr>
		<tr valign="top">
			<td width="8%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">1a</font></font></p>
			</td>
			<td width="31%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">F_GM-DougFir-IT_Sal95.R</font></font></p>
			</td>
			<td width="21%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">SBAI2(AGE,
				N, SBA, SI=SI_def, PD=PD_def, TNP=0)</font></font></p>
				<p><br>
				</p>
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">dbhIncS(d,
				N, DG, SBAI, dbhIncMIN=0.05, b1=SC_b1){</font></font></p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">SBAI2:=
				Stand Basal Area Increment (2: adapted)</font></font></p>
				<p><br>
				</p>
				<p><font face="Liberation Mono, monospace"><font size="2" style="font-size: 10pt">dbhIncS:=
				size class d increment (S: adapted)</font></font></p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
				<p><br>
				</p>
			</td>
		</tr>
		<tr valign="top">
			<td width="8%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="31%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="21%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
				<p><br>
				</p>
			</td>
		</tr>
		<tr valign="top">
			<td width="8%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="31%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="21%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
				<p><br>
				</p>
			</td>
		</tr>
		<tr valign="top">
			<td width="8%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="31%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="21%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
				<p><br>
				</p>
			</td>
			<td width="20%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
				<p><br>
				</p>
			</td>
		</tr>
	</tbody>
</table>
<pre class="western"><br>
</pre>
</body>
</html>