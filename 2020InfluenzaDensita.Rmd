---
author: Roberto Scotti   (NuoroForestrySchool)
date: "`r format(Sys.time(), '%d %B %Y')`"
title: "Accrescimento e produzione delle douglasiete Toscane"
subtitle: Influenza della densità di impianto e di coltivazione
output:
  pdf_document: default
  html_document:
    df_print: paged
keywords: 
abstract: |
  Utilizzando la sintesi dei dati sperimentali raccolti nei decenni precedenti e
  il modello di crescita sviluppato sulla base di tali rilievi, è possibile mettere
  in evidenza come densità di impianto e intensità del diradamento hanno influenzato
  le caratteristiche del popolamento ad età prossime a quelle di possibile utilizzazione
header-includes: \usepackage{subfig}
---

# Introduzione
Le aree sperimentali oggetto di monitoraggio sono state individuate nel contesto di impianti privati e pubblici del Casentino 

```{r}
suppressPackageStartupMessages(library(tidyverse))
library(readODS)
source("Functions/F_SiteIndex_MN94.R")
plot.c <- "InfluenzaDensità/PlotsCharacterization.ods" %>% 
  read_ods(range = "A5:J24", col_names = F) %>% 
  rename(Lotto = A,
         DensImp = B,
         Eta = C,
         h_dom = D,
         hg_su_dg = E,
         DensCorr = F,
         VolCorr = G,
         DirTipo = H,
         DirIntens = I,
         DirVol = J) %>% 
  fill(Lotto) %>% 
  filter(DensImp >0) %>% 
  mutate(hg = h_dom * 0.86,
         dg_da_vol = sqrt((40000/pi)*(VolCorr/DensCorr)/(hg*0.5)),
         dg_da_rapp = 100 * hg / hg_su_dg,
         dg = rowMeans(cbind(dg_da_vol, dg_da_rapp)),
         G = DensCorr * dg^2 * pi/40000,
         SI30 = dougSI_MN94(Eta, h_dom, Eb = 30),
         CF30 = as.factor(3*ceiling((SI30/3)-.5)))

plot.c %>% 
  mutate(Eta = as.factor(Eta)) %>% 
  ggplot() + aes(G, dg, colour = Eta, shape = CF30, size = SI30) +
  geom_point() +
  ggtitle("Fertilità e stato degli impianti all'inizio del monitoraggio")
```

Per produrre una sintesi dei dati sperimentali disponibili, ipotizziamo una batteria situazioni di partenza che copra il range delle condizioni osservate.
Utilizzando il modello simuliamo la cresita nelle condizioni ipotizzate nell'arco delle età esplorate sperimentalmente: da 15 a 40 anni

```{r}
source("Functions/F_GM-DougFir-IT_Sal95.R")
plot.i <- tribble(
  ~Ipotesi, ~SI30, ~DensImp, ~Eta, ~DensCorr, ~G,
  "A1",         27,   1000,      15,   750,      25,
  "A2",         27,   2000,      15,   1400,     30,
  "A3",         27,   3000,      15,   2000,     40,
  "B1",         24,   1000,      15,   800,      25,
  "B2",         24,   2000,      15,   1500,     30,
  "B3",         24,   3000,      15,   2200,     40,
  "C1",         21,   1000,      15,   850,      25,
  "C2",         21,   2000,      15,   1600,     30,
  "C3",         21,   3000,      15,   2500,     40,
) %>% 
  mutate(CF30 = as.factor(3*ceiling((SI30/3)-.5)))

plot.i.out <- plot.i %>% 
  select(Ipotesi, SI30, CF30, DensImp, Eta, DensCorr, G)
while (max(plot.i.out$Eta) <= 40) {
#  cat("Eta corrente: ", max(plot.i.out$Eta) , "\n")
  plot.i.out <- plot.i.out %>% 
    bind_rows(
      plot.i.out %>% 
        filter(Eta == max(plot.i.out$Eta)) %>% 
        mutate(Eta = Eta +1,
               G = G + SBAI(Eta, DensCorr, G, SI30)
               )
    )
}

plot.i.out %>% 
  mutate(DensImp = as.factor(DensImp)) %>% 
  ggplot() + aes(Eta, G, linetype = CF30, color = DensImp) +
  geom_line()+
  ggtitle("Sviluppo dell'area basimetrica", "in funzione della densità di impianto, a diversa fertilità")

```



